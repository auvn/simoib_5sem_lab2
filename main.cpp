#include <iostream>
#include <vector>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <stdio.h>
#include <time.h>

using namespace std;

vector<char> alf;

string encode(string str, string key) {
	string new_str;
	stringstream new_str_ss;
	vector<int> str_key;
	for (unsigned int i = 0, j = 0; i < str.length(); i++, j++) {
		str_key.push_back(key.at(j) - 48);
		if (j + 1 == key.length())
			j = -1;
	}

	int temp_i;
	for (unsigned int i = 0; i < str.length(); i++) {
		temp_i = ((int) str[i]) + str_key.at(i) + 128;
		if (temp_i > 255)
			temp_i -= 255;
		new_str += (alf.at(temp_i));
	}
	return new_str;
}

string input_str() {
	string str;

	cout << "Input str: ";
	while (str[str.size() - 2] != '#') {
		string s;
		cin >> s;
		str += " " + s;
	}

	str.resize(str.size() - 2);
	return str;
}
int seed = 0;
string input_key(int len) {
	string key;
	stringstream ss;
	srand((unsigned int) time(NULL) + seed++);
	for (int i = 0; i < len; i++)
		ss << (rand() % 9) + 1;
	return ss.str();
}

string try_decode(string str, string encode_str) {
	string try_str, try_key;
	stringstream ss;
	for (long i = 0; i < 10000000; i++) {
		ss.str("");
		ss << i;
		try_key = ss.str();
		try_str = encode(str, try_key);

		if (encode_str == try_str) {
			break;
		}
	}
	return try_key;

}
int end, start;
void run_tests() {
	string str;
	string key;
	string encode_str;
	str = input_str();
	int res[3];
	cout
			<< "Key length - Duration1 ms - Duration2 ms - Duration3 ms - Average ms\n";
	for (int i = 3; i <= 7; i++) {
		cout << i << " - ";
		for (int j = 0; j < 3; j++) {
			key = input_key(i);
			encode_str = encode(str, key);
			start = clock();
			try_decode(str, encode_str);
			res[j] = (clock() - start);
			cout << res[j] << " - ";
		}
		cout << (res[0] + res[1] + res[2]) / 3 << "\n";
	}
}

int main(int argc, char **argv) {
	for (int i = -128; i < 128; i++)
		alf.push_back(i);
	run_tests();
	return 1;
}
